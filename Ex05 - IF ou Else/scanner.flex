import java_cup.runtime.Symbol;

%%

%class Scanner
%unicode
%cup
%line
%column

%{
	public Scanner(java.io.InputStream in) {
		this(new java.io.InputStreamReader(in, java.nio.charset.Charset.forName("UTF-8")));
	}
%}

digito = [0-9]
letra = [a-zA-Z]
digitos = [0-9]+
opRelacional = ">"|"<"|">="|"<="|"=="|"!="

fimdeLinha = \r|\n|\r\n
espaco = {fimdeLinha} | [ \t\f]

%%

{digitos} { 
	double aux = Double.parseDouble(yytext());
	return new Symbol(sym.NUMBER, new Double(aux)); 
}

"if" { return new Symbol(sym.KW_IF);}
"else" { return new Symbol(sym.KW_ELSE);}
{opRelacional} {
		String opRelacional = yytext(); 
		return new Symbol(sym.OP_RELACIONAL, opRelacional);
	}
"+" { return new Symbol(sym.MAIS);	}
"-" { return new Symbol(sym.MENOS);}
"/" { return new Symbol(sym.DIV);}
"*" { return new Symbol(sym.MULT);}
"%" { return new Symbol(sym.MOD);}
";" { return new Symbol(sym.PTVIRG);}
"(" { return new Symbol(sym.ABRE_PARENT);}
")" { return new Symbol(sym.FECHA_PARENT);}
"{" { return new Symbol(sym.ABRE_CHAVE);}
"}" { return new Symbol(sym.FECHA_CHAVE);}

{espaco} { /* despreza */ }

.|\n { /* Caractere inválido */ 		
	return new Symbol(sym.EOF, yyline, yycolumn, yytext());		
	}